#pragma once

#include <inttypes.h>

void initDebug();
void setDebug(uint8_t debug);
#ifdef DEBUG_0
void setDebug0(uint8_t on);
#endif
#ifdef DEBUG_1
void setDebug1(uint8_t on);
#endif
#ifdef DEBUG_2
void setDebug2(uint8_t on);
#endif
void setLed(uint8_t on);

