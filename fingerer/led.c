#include "led.h"

#include "light_ws2812.h"


#define LEDS 1



void ledIdle() {
  static uint8_t idleCount = 0;
  static int16_t fade = 0;
  static int8_t dir = 1;
  
  if (idleCount++ > 100) {
    fade += dir;
    idleCount = 0;  

    if (dir > 0 && fade > 50 << 5) {
      dir = -1;
      fade = 50 << 5;  
    }

    if (dir < 0 && fade <= 10 << 5) {
      dir = 1;
      fade = 10 << 5;  
    }
    
    struct cRGB leds[LEDS];
    leds[0].r = 0;
    leds[0].g = fade >> 5;
    leds[0].b = fade >> 6;
    ws2812_setleds(leds,LEDS);    
  }  
}

void ledReadyToCut() {
  struct cRGB leds[LEDS];
  leds[0].r = 0;
  leds[0].g = 100;
  leds[0].b = 0;
  ws2812_setleds(leds,LEDS);
}

void ledRunning() {
  struct cRGB leds[LEDS];
  leds[0].r = 255;
  leds[0].g = 0;
  leds[0].b = 0;
  ws2812_setleds(leds,LEDS);
}

void ledRunningError() {
  struct cRGB leds[LEDS];
  leds[0].r = 255;
  leds[0].g = 0;
  leds[0].b = 0;
  ws2812_setleds(leds,LEDS);
}
